\contentsline {section}{\numberline {1}Revision History}{2}{section.1}
\contentsline {section}{\numberline {2}Introduction}{2}{section.2}
\contentsline {subsection}{\numberline {2.1}Purpose}{2}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Scope}{2}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Assumptions}{2}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}System Boundaries}{2}{subsection.2.4}
\contentsline {subsection}{\numberline {2.5}System Schematic}{2}{subsection.2.5}
\contentsline {subsection}{\numberline {2.6}Functional Decomposition}{2}{subsection.2.6}
\contentsline {subsubsection}{\numberline {2.6.1}Lock}{2}{subsubsection.2.6.1}
\contentsline {subsubsection}{\numberline {2.6.2}Unlock}{2}{subsubsection.2.6.2}
\contentsline {subsubsection}{\numberline {2.6.3}Authorize user}{3}{subsubsection.2.6.3}
\contentsline {subsubsection}{\numberline {2.6.4}Revoke user}{3}{subsubsection.2.6.4}
\contentsline {subsubsection}{\numberline {2.6.5}Authorize guest}{3}{subsubsection.2.6.5}
\contentsline {subsubsection}{\numberline {2.6.6}Revoke guest}{3}{subsubsection.2.6.6}
\contentsline {subsubsection}{\numberline {2.6.7}Log in}{3}{subsubsection.2.6.7}
\contentsline {subsubsection}{\numberline {2.6.8}Register}{3}{subsubsection.2.6.8}
\contentsline {subsubsection}{\numberline {2.6.9}Add lock}{3}{subsubsection.2.6.9}
\contentsline {subsubsection}{\numberline {2.6.10}Delete lock}{4}{subsubsection.2.6.10}
\contentsline {section}{\numberline {3}FMEA Chart}{5}{section.3}
