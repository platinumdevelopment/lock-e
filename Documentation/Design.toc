\contentsline {part}{I\hspace {1em}Revision History}{2}{part.1}
\contentsline {section}{\numberline {1}Purpose}{3}{section.1}
\contentsline {section}{\numberline {2}Validation}{3}{section.2}
\contentsline {subsection}{\numberline {2.1}Relationship Between Isolated Tests and Requirements}{3}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Tracing isolated tests to design decisions and requirements}{3}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Integrated testing approach}{3}{subsection.2.3}
\contentsline {section}{\numberline {3}Verification}{3}{section.3}
\contentsline {subsection}{\numberline {3.1}Isolated Testing}{3}{subsection.3.1}
\contentsline {subsubsection}{\numberline {3.1.1}Web Service}{3}{subsubsection.3.1.1}
\contentsline {subsubsection}{\numberline {3.1.2}Android Application Testing}{7}{subsubsection.3.1.2}
\contentsline {subsubsection}{\numberline {3.1.3}Hardware Testing}{9}{subsubsection.3.1.3}
\contentsline {subsection}{\numberline {3.2}Integrated Testing}{10}{subsection.3.2}
