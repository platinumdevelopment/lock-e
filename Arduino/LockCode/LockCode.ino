#include <Wire.h>
#include <SPI.h>
#include <Adafruit_PN532.h>
#include <Stepper.h>
#include <ctype.h>

// If using the shield with I2C, define just the pins connected
// to the IRQ and reset lines.  Use the values below (2, 3) for the shield!
#define PN532_IRQ   (2)
#define PN532_RESET (3)

// Constant settings
#define MESSAGE_LENGTH_BYTES 15
#define LOCK_KEY_LENGTH 10
#define ENCRYPTION_KEY_LENGTH 25
#define ENCRYPTION_KEY "y64yszya6ya4w64w632tuiog0"

// Lock-specific settings
#define LOCK_KEY "ihu2r4149a"
#define LOCK_APDU_AID 0xF2, 0x22, 0x22, 0x22, 0x22


const int k_stepsPerRevolution = 512; //our motor has 512 steps per revolution
uint64_t current_key_num = 0;
bool toggleLock = true;

//Initializing stepper library on pins 8-11
Stepper myStepper(k_stepsPerRevolution, 11, 9, 10, 8);

Adafruit_PN532 nfc(PN532_IRQ, PN532_RESET);



//                      dir   lock key               number
// char my_message [] = "0 bisgtbeitbekhou2r4419489a 0012"

// Decrypts the encrypted message from the android app
char * decrypt(char * message) {
    const char encryption_key [] = ENCRYPTION_KEY;
    char decrypted_message [MESSAGE_LENGTH_BYTES];
    for (size_t i = 0; i < MESSAGE_LENGTH_BYTES; i++) {
        decrypted_message[i] = message[i] - encryption_key[i % ENCRYPTION_KEY_LENGTH];
    }
    return decrypted_message;
}

// Given the decrypted message, this function checks if a key is valid
bool check_key(char * message) {
    const char lock_key [] = LOCK_KEY;

    bool key_matches = true;
    for (size_t i = 0; i < LOCK_KEY_LENGTH; i++) {
        if (message[i + 1] != lock_key[i]) {
            key_matches = false;
            break;
        }
    }

    bool num_okay = false;
    uint32_t this_num = 0;
    for (size_t i = 0; i < 4; i++) {
        this_num += (uint32_t)message[LOCK_KEY_LENGTH + i + 1] << (i * 8);
    }
    if (this_num > current_key_num) {
        num_okay = true;
    }

    if (key_matches && num_okay) {
        current_key_num = this_num;
        return true;
    }
    else {
        return false;
    }
}


// Given the message from the android app, this says which direction to turn the motor
bool get_direction(char * message) {
    return (message[0] == '1');
}

void setup(void) {
    Serial.begin(115200);
    Serial.println("Hello!");

    myStepper.setSpeed(30); //set motor speed to 30rpm
    nfc.begin();
    
    uint32_t versiondata = nfc.getFirmwareVersion();
    if (! versiondata) {
        Serial.print("Didn't find PN53x board");
        while (1); // halt
    }
    
    // Got ok data, print it out!
    Serial.print("Found chip PN5"); Serial.println((versiondata>>24) & 0xFF, HEX);
    Serial.print("Firmware ver. "); Serial.print((versiondata>>16) & 0xFF, DEC);
    Serial.print('.'); Serial.println((versiondata>>8) & 0xFF, DEC);
    
    // Set the max number of retry attempts to read from a card
    // This prevents us from waiting forever for a card, which is
    // the default behaviour of the PN532.
    nfc.setPassiveActivationRetries(0xFF);
    
    // configure board to read RFID tags
    nfc.SAMConfig();
}



void loop()
{
    bool success;
    uint8_t responseLength = 20;
    char keyArray [] = LOCK_KEY; // key that will be used 
                                 // for authentication
    
    Serial.println("Waiting for an ISO14443A card");
    // set shield to inListPassiveTarget
    success = nfc.inListPassiveTarget();
    if(success)
    {
        Serial.println("Found something!");
        uint8_t selectApdu[] = { 0x00, /* CLA */
            0xA4, /* INS */
            0x04, /* P1  */
            0x00, /* P2  */
            0x05, /* Length of AID  */
            LOCK_APDU_AID /* AID defined on Android App */
            };
            
        uint8_t response[10];
        success = nfc.inDataExchange(selectApdu, sizeof(selectApdu), response, &responseLength);
        
        
        if(success) 
        {
            Serial.print("responseLength: "); Serial.println(responseLength);
            for (uint8_t pos = 0; pos < responseLength-2; pos += MESSAGE_LENGTH_BYTES)
            {
                char *thisMsg = &response[pos];
                char *decrypted_message = decrypt(thisMsg);
                if (check_key(decrypted_message)) {
                    // Valid key, move motor
                    bool direction = get_direction(decrypted_message);
                    if (direction) {
                        myStepper.step(k_stepsPerRevolution / 2);
                    }
                    else {
                        myStepper.step(-k_stepsPerRevolution / 2);   
                    }
                }
            }
        }
        else 
        {
            Serial.println("Failed sending SELECT AID");
        }

    }
    else 
    {
        Serial.println("Didn't find anything!");
    }
    delay(1000);
}


