#include <Wire.h>
#include <SPI.h>
#include <Adafruit_PN532.h>

// If using the breakout with SPI, define the pins for SPI communication.
#define PN532_SCK  (2)
#define PN532_MOSI (3)
#define PN532_SS   (4)
#define PN532_MISO (5)

// If using the breakout or shield with I2C, define just the pins connected
// to the IRQ and reset lines.  Use the values below (2, 3) for the shield!
#define PN532_IRQ   (2)
#define PN532_RESET (3)  // Not connected by default on the NFC Shield

Adafruit_PN532 nfc(PN532_IRQ, PN532_RESET);

void setup(void) {
    Serial.begin(115200);
    Serial.println("Hello!");
    
    nfc.begin();
    
    uint32_t versiondata = nfc.getFirmwareVersion();
    if (! versiondata) {
        Serial.print("Didn't find PN53x board");
        while (1); // halt
    }
    
    // Got ok data, print it out!
    Serial.print("Found chip PN5"); Serial.println((versiondata>>24) & 0xFF, HEX);
    Serial.print("Firmware ver. "); Serial.print((versiondata>>16) & 0xFF, DEC);
    Serial.print('.'); Serial.println((versiondata>>8) & 0xFF, DEC);
    
    // Set the max number of retry attempts to read from a card
    // This prevents us from waiting forever for a card, which is
    // the default behaviour of the PN532.
    nfc.setPassiveActivationRetries(0xFF);
    
    // configure board to read RFID tags
    nfc.SAMConfig();
    
    Serial.println("Waiting for an ISO14443A card");
}



void loop()
{
    bool success;
    uint8_t responseLength = 10;
    uint8_t keyArray [] = {'1','2','3','4','5','6','7','8'}; //key that will be used for authentication

    bool keyVerified = true;
    
    Serial.println("Waiting for an ISO14443A card");
    // set shield to inListPassiveTarget
    success = nfc.inListPassiveTarget();
    if(success) {
        Serial.println("Found something!");
        uint8_t selectApdu[] = { 0x00, /* CLA */
            0xA4, /* INS */
            0x04, /* P1  */
            0x00, /* P2  */
            0x05, /* Length of AID  */
            0xF2, 0x22, 0x22, 0x22, 0x22 //, 0x05, 0x06, /* AID defined on Android App */
            /*0x00*/  /* Le  */ };
        uint8_t response[10];
        success = nfc.inDataExchange(selectApdu, sizeof(selectApdu), response, &responseLength);
        
        
        if(success) {
            Serial.print("responseLength: "); Serial.println(responseLength);
           // nfc.PrintHexChar(response, responseLength);

            for (uint8_t szPos=0; szPos < responseLength-2; szPos++)
            {
                if (response[szPos] != keyArray[szPos]){
                  keyVerified = false;
                  break;
                }
                Serial.print((char)response[szPos]);
                Serial.println();
            }
            if (keyVerified){
              Serial.print("Verified!");
            }else{
              Serial.print("Failed!");
              keyVerified = true;
            }
            Serial.println();
        }
        else {
            Serial.println("Failed sending SELECT AID");
        }
    }
    else {
        Serial.println("Didn't find anything!");
    }
    delay(1000);
}


void printResponse(uint8_t *response, uint8_t responseLength) {
    String respBuffer;
    for (int i = 0; i < responseLength; i++) {
        if (response[i] < 0x10)
            respBuffer = respBuffer + "0"; //Adds leading zeros if hex value is smaller than 0x10
        respBuffer = respBuffer + String(response[i], HEX) + " ";
    }
    Serial.print("real response: "); Serial.println(respBuffer);
}

