package com.example.capstone;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

public class MyLocks extends Activity {
	
	static String token = "", userInfoUrl = "", http_return = "";
	HttpRequest httprequest;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_my_locks);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		setTitle("My Locks");

		SharedPreferences prefs = getSharedPreferences("key", MODE_PRIVATE);
		token = prefs.getString("key_token", null);

		userInfoUrl = "https://tranquil-caverns-74038.herokuapp.com/LocknLoad/getLocks/token="
				+ token + "/";

		httprequest = new HttpRequest();

		http_return = httprequest.getFromURL(userInfoUrl);

		// While http return value hasn't changed, loop until it does
		while (http_return.equals("")) {

			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		if (http_return.trim().equals("-1")) {

			Toast.makeText(getApplicationContext(),
					"Error, token expired", Toast.LENGTH_LONG).show();

		} else {

			// Parsing JSON Values
			try {
				JSONObject object = new JSONObject(http_return);
				JSONArray owner = object.getJSONArray("owner");
				JSONArray tenant = object.getJSONArray("tenant");
				JSONArray guest = object.getJSONArray("guest");

				Lock[] ownerList = new Lock[owner.length()];
				Lock[] tenantList = new Lock[tenant.length()];
				Lock[] guestList = new Lock[guest.length()];

				LinearLayout ownerLocks = (LinearLayout) findViewById(R.id.linearLayout12);
				LinearLayout tenantLocks = (LinearLayout) findViewById(R.id.linearLayout22);
				LinearLayout guestLocks = (LinearLayout) findViewById(R.id.linearLayout32);

				ownerLocks.removeAllViews();
				tenantLocks.removeAllViews();
				guestLocks.removeAllViews();

				for (int i = 0; i < owner.length(); i++) {

					Lock lock = new Lock(this);
					lock.setLockId(owner.getJSONObject(i).getString("lock_id"));
					lock.setLockName(owner.getJSONObject(i).getString("name"));
					lock.setRelationship(0);
					lock.setText(lock.getLockName());
					lock.setTextSize(22);
					lock.setTextColor(Color.rgb(0, 174, 239));
		            lock.setPadding(40, 0, 0, 0);
					ownerLocks.addView(lock);

				}

				for (int i = 0; i < tenant.length(); i++) {

					Lock lock = new Lock(this);
					lock.setLockId(tenant.getJSONObject(i).getString("lock_id"));
					lock.setLockName(tenant.getJSONObject(i).getString("name"));
					lock.setRelationship(1);
					lock.setText(lock.getLockName());
					lock.setTextSize(22);
		            lock.setTextColor(Color.rgb(0, 174, 239));
		            lock.setPadding(40, 0, 0, 0);
					tenantLocks.addView(lock);

				}

				for (int i = 0; i < guest.length(); i++) {

					Lock lock = new Lock(this);
					lock.setLockId(guest.getJSONObject(i).getString("lock_id"));
					lock.setLockName(guest.getJSONObject(i).getString("name"));
					lock.setRelationship(2);
					lock.setText(lock.getLockName());

					guestLocks.addView(lock);

				}



			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}
	

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home) {
			Intent intent = new Intent(this, MainActivity.class);
			startActivity(intent);
			finish();
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu items for use in the action bar
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.mylocks, menu);
		return super.onCreateOptionsMenu(menu);
	}

	public void addNewLock(MenuItem item) {

		Intent intent = new Intent(this, AddNewLock.class);
		intent.putExtra("token", token);
		startActivity(intent);

	}

	public void goToOwnedLock(View view) {

		Intent intent = new Intent(this, OwnedLocks.class);
		startActivity(intent);
	}

	//IF BACK BUTTON IS PRESSED
	@Override
	public void onBackPressed() {
		Intent intent = new Intent(this, MainActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(intent);
		finish();
	}


}