package com.example.capstone;

import android.R;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class LockUser extends LinearLayout {

	private String userId;
	private TextView email;
	private Button delete;
	private String lockId;
	private String token;

	public LockUser(Context context) {
		super(context);

		LayoutParams emailParams = new LayoutParams(LayoutParams.WRAP_CONTENT,
				LayoutParams.WRAP_CONTENT);
		LayoutParams deleteParams = new LayoutParams(LayoutParams.WRAP_CONTENT,
				LayoutParams.WRAP_CONTENT);

		emailParams.weight = (float) 0.9;

		email = new TextView(context);
		delete = new Button(context);

		email.setLayoutParams(emailParams);
		email.setTextSize(22);
		email.setTextColor(Color.rgb(0, 174, 239));
		email.setPadding(40, 0, 0, 0);

		delete.setLayoutParams(deleteParams);
		delete.setBackgroundResource(R.drawable.ic_delete);
		delete.isClickable();

		delete.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Log.e("buttontest", "inOnClick");

				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
						v.getContext());
				alertDialogBuilder.setTitle("Delete Confirmation");

				alertDialogBuilder
						.setMessage("Are you sure you want to delete "
								+ email.getText().toString() + " ?");
				alertDialogBuilder.setCancelable(false);
				alertDialogBuilder.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {

							public void onClick(DialogInterface dialog, int id) {

								String deleteUrl = "https://tranquil-caverns-74038.herokuapp.com/LocknLoad/revokeAuthorization/token=" + token + "&user_to_revoke="
										+ userId + "&lock_id=" + lockId + "/";

								HttpRequest httprequest = new HttpRequest();
								String http_return = httprequest
										.getFromURL(deleteUrl);

								while (http_return.equals("")) {

									try {
										Thread.sleep(10);
									} catch (InterruptedException e) {
										e.printStackTrace();
									}
								}

								if (http_return.contains("Successfully removed authorization!")) {

									Intent intent = new Intent(LockUser.this
											.getContext(), OwnedLocks.class);
									intent.putExtra("lockId", OwnedLocks.lockId);
									intent.putExtra("lockName", OwnedLocks.lockName);
									intent.putExtra("relationship", OwnedLocks.relationship);
									LockUser.this.getContext().startActivity(
											intent);
								} else if (http_return.trim().equals("-1")) {
									Toast.makeText(LockUser.this.getContext(),
											"Error, token expired", Toast.LENGTH_LONG).show();

								} else {

									Toast.makeText(LockUser.this.getContext(),
											"Error" + http_return, Toast.LENGTH_LONG).show();

								}
							}

						});
				alertDialogBuilder.setNegativeButton("Cancel",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								// Cancels Lobby Choice
							}
						});

				AlertDialog alertDialog = alertDialogBuilder.create();

				alertDialog.show();
			}
		});

		this.addView(email);
		this.addView(delete);

	}

	public void setEmail(String email) {
		this.email.setText(email);
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	public void setLockId(String lockId) {
		this.lockId = lockId;
	}

	public void setToken(String token) {this.token = token; }

}
