package com.example.capstone;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import android.os.AsyncTask;
import android.util.Log;


public class HttpRequest {

String url_to_task = "";
String string_result = "hasn't returned a value";

		protected String getFromURL (String url) {
			url_to_task = url;
			new MyTask().execute();
			while (string_result.equals("hasn't returned a value")) {
				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			return string_result;
		}
		
		private class MyTask extends AsyncTask <Void, Void, Void> {
			
			
			@Override
			protected Void doInBackground(Void... params) {
				
				InputStream is = null;
				
				//Download JSON data from url
				try {
					HttpClient httpclient = new DefaultHttpClient();
					HttpPost httppost = new HttpPost(url_to_task);
					httppost.addHeader("Referer", "https://tranquil-caverns-74038.herokuapp.com/");
					
					HttpResponse response = httpclient.execute(httppost);
					HttpEntity entity = response.getEntity();
					is = entity.getContent();
					
				} catch (Exception e) {
					
					Log.e("log_tag", "Error in http connection " + e.toString());
					
				}
				
				//Convert response to string
				
				try {
					BufferedReader reader = new BufferedReader (new InputStreamReader(is, "iso-8859-1"), 8);
					StringBuilder sb = new StringBuilder();
					String line = null;
					while ((line = reader.readLine()) != null) {
						sb.append(line + "\n");
						
					}
					is.close();
					string_result = sb.toString();
				} catch (Exception e) {
					Log.e("log_tag", "Error converting result" + e.toString());
				}
				

				return null;
			
			}
			
			@Override
			protected void onPostExecute(Void result) {				
				super.onPostExecute(result);
			}
			

		}
}
