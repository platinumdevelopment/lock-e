package com.example.capstone;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class AddNewLock extends Activity {

	String token = "";
			
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_new_lock);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		setTitle("Add New Lock");

		RadioGroup radioGroup = (RadioGroup) findViewById(R.id.radioGroup1);

		RadioButton lockCC = (RadioButton) findViewById(R.id.radioButtonCounterClock);
		RadioButton lockC = (RadioButton) findViewById(R.id.radioButtonClock);

		radioGroup.check(R.id.radioButtonCounterClock);
		
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home) {
			Intent intent = new Intent(this, MyLocks.class);
		  	startActivity(intent);
		  	finish();
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu items for use in the action bar
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.addnewlock, menu);
		return super.onCreateOptionsMenu(menu);
	}

	//IF BACK BUTTON IS PRESSED
	@Override
	public void onBackPressed() {
		Intent intent = new Intent(this, MyLocks.class);
		startActivity(intent);
		finish();
	}
	
	public void finishAddingNewLock(MenuItem item) {

		SharedPreferences prefs = getSharedPreferences("key", MODE_PRIVATE);
		String token = prefs.getString("key_token", null);

		String http_return = "";
		
		String lockName = (String) ((EditText)findViewById(R.id.newlockname)).getText().toString();
		String lockId = (String) ((EditText)findViewById(R.id.newlockid)).getText().toString();

        String direction = "";
        RadioGroup radioGroup = (RadioGroup) findViewById(R.id.radioGroup1);

        int id = radioGroup.getCheckedRadioButtonId();
        if (id == -1) {
            // no item selected
        } else {
            if (id == R.id.radioButtonCounterClock) {
                direction = "0";
            } else if (id == R.id.radioButtonClock) {
                direction = "1";
            }
        }
		
		if (lockName.equals("") || lockId.equals("")) {
        Toast.makeText(getApplicationContext(), "Fill in both fields!",
                Toast.LENGTH_LONG).show();

        } else {

			String addLockUrl = "https://tranquil-caverns-74038.herokuapp.com/LocknLoad/addLock/token=<OWNER>&name=<LOCKNAME>&id=<ID>&apdu=<APDU>&direction=<DIRECTION>/";
			addLockUrl = addLockUrl.replace("<OWNER>", token);
			addLockUrl = addLockUrl.replace("<LOCKNAME>", lockName);
			addLockUrl = addLockUrl.replace("<ID>", lockId);
			addLockUrl = addLockUrl.replace("<APDU>", "test");
			addLockUrl = addLockUrl.replace("<DIRECTION>", direction);
		
			HttpRequest httprequest = new HttpRequest();
			http_return = httprequest.getFromURL(addLockUrl);

			while (http_return.equals("")) {

				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		
			if (http_return.contains("Lock registered successfully!")) {
			
				Intent intent = new Intent(this, MyLocks.class);
				intent.putExtra("token", token);
				startActivity(intent);
				finish();
			
			
			} else if (http_return.contains("A lock with that id already exists")) {

				Toast.makeText(getApplicationContext(),
						http_return, Toast.LENGTH_LONG).show();

			} else if (http_return.trim().equals("-1")) {
				Toast.makeText(getApplicationContext(),
						"Error, token expired", Toast.LENGTH_LONG).show();

			} else {
			
				//Shouldn't come here
				Toast.makeText(getApplicationContext(),
						"Error" + http_return, Toast.LENGTH_LONG).show();
			}


        }

		

	}
}
