package com.example.capstone;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class OwnedLocks extends Activity {

	static String token = "", lockId = "", lockName = "";
	static int relationship;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_owned_locks);
		setTitle("Owned Lock");

		SharedPreferences prefs = getSharedPreferences("key", MODE_PRIVATE);
		token = prefs.getString("key_token", null);

		lockId = getIntent().getStringExtra("lockId");
		lockName = getIntent().getStringExtra("lockName");
		relationship = getIntent().getIntExtra("relationship", 99);

		String lockInfoUrl = "https://tranquil-caverns-74038.herokuapp.com/LocknLoad/getUsersUnder/token="
				+ token + "&lock_id=" + lockId + "/";
		HttpRequest httprequest = new HttpRequest();
		String http_return = httprequest.getFromURL(lockInfoUrl);

		while (http_return.equals("")) {

			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		if (http_return.trim().equals("-1")) {

			Toast.makeText(getApplicationContext(),
					"Error, token expired", Toast.LENGTH_LONG).show();

		} else {

			try {
				JSONObject object = new JSONObject(http_return);
				JSONArray tenant = object.getJSONArray("tenants");
				JSONArray guest = object.getJSONArray("guests");

				LinearLayout tenants = (LinearLayout) findViewById(R.id.tenantList);
				LinearLayout guests = (LinearLayout) findViewById(R.id.guestList);

				for (int i = 0; i < tenant.length(); i++) {

					LayoutParams params = new LayoutParams(
							LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
					LockUser user = new LockUser(this);
					user.setLayoutParams(params);

					user.setEmail(tenant.getJSONObject(i).getString("email"));
					user.setUserId(tenant.getJSONObject(i).getString("id"));
					user.setLockId(lockId);
					user.setToken(token);
					tenants.addView(user);

				}

				for (int i = 0; i < guest.length(); i++) {

					LayoutParams params = new LayoutParams(
							LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
					LockUser user = new LockUser(this);
					user.setLayoutParams(params);

					user.setEmail(guest.getJSONObject(i).getString("email"));
					user.setUserId(guest.getJSONObject(i).getString("id"));
					user.setLockId(lockId);
					user.setToken(token);
					guests.addView(user);

				}

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			TextView lockNameTV = (TextView) findViewById(R.id.lockTitle);
			lockNameTV.setText(lockName);

			if (!(relationship == 0)) {

				Button deleteButton = (Button) findViewById(R.id.deleteButton);
				deleteButton.setVisibility(View.INVISIBLE);
			}
		}


	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home) {
			finish();
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onBackPressed() {

		Intent intent = new Intent(this, MyLocks.class);
		intent.putExtra("token", token);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(intent);

	}

	public void deleteLock(View view) {

		String deleteLockUrl = "https://tranquil-caverns-74038.herokuapp.com/LocknLoad/deleteLock/token=" + token + "&lock_id="
				+ lockId + "/";

		HttpRequest httprequest = new HttpRequest();

		String http_return = httprequest.getFromURL(deleteLockUrl);

		// While http return value hasn't changed, loop until it does
		while (http_return.equals("")) {

			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		if (http_return.contains("Lock deleted successfully")) {

			Intent intent = new Intent(this, MyLocks.class);
			intent.putExtra("token", token);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);

		} else if (http_return.trim().equals("-1")) {

			Toast.makeText(getApplicationContext(),
					"Error, token expired", Toast.LENGTH_LONG).show();

		} else {

			Toast.makeText(getApplicationContext(), "Error" + http_return,
					Toast.LENGTH_LONG).show();

		}

	}

	public void addTenant(View view) {

		AlertDialog.Builder alert = new AlertDialog.Builder(this);

		alert.setTitle("Add Tenant");
		alert.setMessage("Enter Tenant Information...");

		// Set an EditText view to get user input
		final EditText input = new EditText(this);
		alert.setView(input);

		alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {

				String email = input.getText().toString();
				String addTenantUrl = "https://tranquil-caverns-74038.herokuapp.com/LocknLoad/addAuthorization/token="
						+ token
						+ "&user_to_add="
						+ email
						+ "&user_to_add_type=tenant&lock_id=" + lockId + "/";

				HttpRequest httprequest = new HttpRequest();

				String http_return = httprequest.getFromURL(addTenantUrl);

				Log.e("addUrl1", addTenantUrl);

				// While http return value hasn't changed, loop until it does
				while (http_return.equals("")) {

					try {
						Thread.sleep(10);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}

				if (http_return.contains("Successfully authorized tenant!")) {

					finish();
					startActivity(getIntent());

				} else if (http_return.trim().equals("-1")) {

					Toast.makeText(getApplicationContext(),
							"Error, token expired", Toast.LENGTH_LONG).show();

				} else {

					Toast.makeText(getApplicationContext(),
							"Error" + http_return, Toast.LENGTH_LONG).show();

				}

			}
		});

		alert.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						// Canceled.
					}
				});

		alert.show();

	}

	public void addGuest(View view) {

		AlertDialog.Builder alert = new AlertDialog.Builder(this);

		alert.setTitle("Add Guest");
		alert.setMessage("Enter Guest Information...");

		// Set an EditText view to get user input
		final EditText input = new EditText(this);
		alert.setView(input);

		alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {

				String addGuestUrl = "";

				String email = input.getText().toString();

				addGuestUrl = "https://tranquil-caverns-74038.herokuapp.com/LocknLoad/addAuthorization/token="
						+ token
						+ "&user_to_add="
						+ email
						+ "&user_to_add_type=guest&lock_id=" + lockId + "/";

				Log.e("addUrl2", addGuestUrl);

				HttpRequest httprequest = new HttpRequest();

				String http_return = httprequest.getFromURL(addGuestUrl);

				// While http return value hasn't changed, loop until it does
				while (http_return.equals("")) {

					try {
						Thread.sleep(10);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}

				if (http_return.contains("Successfully authorized guest!")) {

					finish();
					startActivity(getIntent());

				} else if (http_return.trim().equals("-1")) {

					Toast.makeText(getApplicationContext(),
							"Error, token expired", Toast.LENGTH_LONG).show();

				} else {

					Toast.makeText(getApplicationContext(),
							"Error" + http_return, Toast.LENGTH_LONG).show();

				}

			}
		});

		alert.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						// Canceled.
					}
				});

		alert.show();

		String addGuesttUrl = "";
	}
}