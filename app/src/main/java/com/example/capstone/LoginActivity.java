package com.example.capstone;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

/*
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.crypto.MacProvider; */

public class LoginActivity extends Activity {


	@Override
	protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.login, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void send_login(View view) {

		String email = "";
		String password = "";

		String loginUrl1 = "https://tranquil-caverns-74038.herokuapp.com/LocknLoad/login/email=";
		String loginUrl2 = "&password=";


		HttpRequest httprequest = new HttpRequest();
		String http_return = "";

		EditText email_input = (EditText) findViewById(R.id.email);
		EditText password_input = (EditText) findViewById(R.id.password);

/* /This line will throw an exception if it is not a signed JWS (as expected)
			String text = "616161";
			//Key key = ;
				Claims claims = Jwts.parser()
					.setSigningKey(Base64.decode(text, Base64.DEFAULT))
					.parseClaimsJws("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJodHRwczovL2p3dC1pZHAuZXhhbXBsZS5jb20iLCJzdWIiOiJtYWlsdG86bWlrZUBleGFtcGxlLmNvbSIsIm5iZiI6MTQ1NjY4ODQ5MCwiZXhwIjoxNDU2NjkyMDkwLCJpYXQiOjE0NTY2ODg0OTAsImp0aSI6ImlkMTIzNDU2IiwidHlwIjoiaHR0cHM6Ly9leGFtcGxlLmNvbS9yZWdpc3RlciJ9.l420Mgi8HccQsO8IHJfFjRjEC7T9nfyTRUA7imd2Ir0").getBody();
		System.out.println("ID: " + claims.getId());
			System.out.println("Subject: " + claims.getSubject());
			System.out.println("Issuer: " + claims.getIssuer());
			System.out.println("Expiration: " + claims.getExpiration());*/


// We need a signing key, so we'll create one just for this example. Usually
// the key would be read from your application configuration instead.
//		Key key = MacProvider.generateKey();

//		String s = Jwts.builder().setSubject("secret").signWith(SignatureAlgorithm.HS256, key).compact();
//		Claims claims =  Jwts.parser().setSigningKey(key).parseClaimsJws(s).getBody();



		// Convert variables to strings
		email = email_input.getText().toString().trim();
		password = password_input.getText().toString().trim();

		// Ensure fields aren't empty
		if (email.equals("") && password.equals("")) {
	//		Toast.makeText(getApplicationContext(),
	//				claims.getSubject(), Toast.LENGTH_LONG).show();

		} else if (email.equals("")) {
			Toast.makeText(getApplicationContext(), "Email is blank!",
					Toast.LENGTH_LONG).show();

		} else if (password.equals("")) {
			Toast.makeText(getApplicationContext(), "Password is blank!",
					Toast.LENGTH_LONG).show();

		} else {

			// If info is valid

			loginUrl1 = loginUrl1 + email + loginUrl2 + password + "/";
			http_return = httprequest.getFromURL(loginUrl1).trim();

			// While http return value hasn't changed, loop until it does
			while (http_return.equals("")) {

				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}

			if (!http_return.startsWith("-1")) {

				Intent intent = new Intent(this, MainActivity.class);
				SharedPreferences.Editor editor  =  getSharedPreferences("key", MODE_PRIVATE).edit();
				editor.putString("key_token", http_return);
				editor.commit();
				startActivity(intent);
				finish();
			} else {

				Toast.makeText(getApplicationContext(),
						"Email or Password is incorrect...", Toast.LENGTH_LONG).show();

			}

		}
	}

	public void send_register(View view) {

		Intent intent = new Intent(this, RegisterActivity.class);
		startActivity(intent);
		finish();
		
	}



}
