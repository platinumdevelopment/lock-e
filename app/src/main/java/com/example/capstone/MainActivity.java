package com.example.capstone;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends Activity {
	
    String getLocksURL = "https://tranquil-caverns-74038.herokuapp.com/LocknLoad/getLocks/token=<TOKEN>/";
	String getKeyURL = "https://tranquil-caverns-74038.herokuapp.com/LocknLoad/getKey/token=<TOKEN>&lock_id=<LOCK_ID>/";
	String tokenValidURL = "https://tranquil-caverns-74038.herokuapp.com/LocknLoad/hasExpired/token=<TOKEN>/";
    String token = "";
	String prefLockId = "";
	Spinner lockSpinner;
	List<String> lockIdList = new ArrayList<>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	    //Below line adds back button on top left of menu bar
		getActionBar().setDisplayHomeAsUpEnabled(true);

		SharedPreferences prefs = getSharedPreferences("key", MODE_PRIVATE);
		token = prefs.getString("key_token", null);

		if (token == null) {

			token = "";
		}

		lockSpinner = (Spinner) findViewById(R.id.locks_spinner);
		List<String> lockList = new ArrayList<>();

		tokenValidURL = tokenValidURL.replace("<TOKEN>", token);

		HttpRequest tokenCheck = new HttpRequest();
		String http_return_valid_token = tokenCheck.getFromURL(tokenValidURL).trim();

		if(token.equals("") || http_return_valid_token.equals("-1"))
		{
			Intent intent = new Intent(this, LoginActivity.class);
			startActivity(intent);
			finish();
		}

		getLocksURL = getLocksURL.replace("<TOKEN>", token);

		//Get locks from URL
		HttpRequest httprequest = new HttpRequest();
		String http_return_locks = httprequest.getFromURL(getLocksURL).trim();

		try {
			JSONObject lock_object = new JSONObject(http_return_locks);
			JSONArray owner = lock_object.getJSONArray("owner");
			JSONArray tenant = lock_object.getJSONArray("tenant");
			JSONArray guest = lock_object.getJSONArray("guest");

			for (int i = 0; i < owner.length(); i++) {

				lockList.add(owner.getJSONObject(i).getString("name"));
				lockIdList.add(owner.getJSONObject(i).getString("lock_id"));
			}

			for (int i = 0; i < tenant.length(); i++ ) {

				lockList.add(tenant.getJSONObject(i).getString("name"));
				lockIdList.add(tenant.getJSONObject(i).getString("lock_id"));

			}

			for (int i = 0; i < guest.length(); i++) {

				lockList.add(guest.getJSONObject(i).getString("name"));
				lockIdList.add(guest.getJSONObject(i).getString("lock_id"));


			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		ArrayAdapter<String> adapter = new ArrayAdapter<String>(
				this, android.R.layout.simple_spinner_item, lockList);

		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		lockSpinner.setAdapter(adapter);

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home) {
			final Intent intent = new Intent(this, LoginActivity.class);

			AlertDialog.Builder logoutDialog = new AlertDialog.Builder(MainActivity.this);

			logoutDialog.setTitle("Logout"); // Sets title for your alertbox

			logoutDialog.setMessage("Are you sure you want to Logout ?");

			logoutDialog.setIcon(android.R.drawable.ic_dialog_alert);

		/* When positive (yes/ok) is clicked */
			logoutDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					SharedPreferences.Editor editor  =  getSharedPreferences("key", MODE_PRIVATE).edit();
					editor.putString("key_token", "");
					editor.commit();

					Toast.makeText(MainActivity.this, "Successfully Logged Out", Toast.LENGTH_LONG).show();
					startActivity(intent);
					finish();
				}
			});

		/* When negative (No/cancel) button is clicked */
			logoutDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					dialog.cancel();
				}
			});
			logoutDialog.show();
		}
		return super.onOptionsItemSelected(item);
	}

	//IF BACK BUTTON IS PRESSED
	@Override
	public void onBackPressed() {
		final Intent intent = new Intent(this, LoginActivity.class);

		AlertDialog.Builder logoutDialog = new AlertDialog.Builder(MainActivity.this);

		logoutDialog.setTitle("Logout"); // Sets title for your alertbox

		logoutDialog.setMessage("Are you sure you want to Logout ?");

		logoutDialog.setIcon(android.R.drawable.ic_dialog_alert);

		/* When positive (yes/ok) is clicked */
		logoutDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				SharedPreferences.Editor editor  =  getSharedPreferences("key", MODE_PRIVATE).edit();
				editor.putString("key_token", "");
				editor.commit();

				Toast.makeText(MainActivity.this, "Successfully Logged Out", Toast.LENGTH_LONG).show();
				startActivity(intent);
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				finish();
			}
		});

		/* When negative (No/cancel) button is clicked */
		logoutDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}
		});
		logoutDialog.show();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu items for use in the action bar
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	public void goToMyLocks(MenuItem item) {

		Intent intent = new Intent(this, MyLocks.class);

	 	startActivity(intent);
	 	
	}
	public void lock(View view) {

		int selectedIndex = lockSpinner.getSelectedItemPosition();
		String lockId = lockIdList.get(selectedIndex);

		String requestURL = getKeyURL.replace("<TOKEN>", token);
		requestURL = requestURL.replace("<LOCK_ID>", lockId);

        //Get keys from URL
        HttpRequest httprequest = new HttpRequest();
        String lockKey = httprequest.getFromURL(requestURL).trim();

		Toast.makeText(getApplicationContext(),
				lockKey, Toast.LENGTH_SHORT).show();

		Log.e("url", requestURL);

		// Set account with the key the user is authorized to use
		AccountStorage.SetAccount(MainActivity.this, lockKey);
	}

	public void unlock(View view) {

		int selectedIndex = lockSpinner.getSelectedItemPosition();
		String lockId = lockIdList.get(selectedIndex);

		String requestURL = getKeyURL.replace("<TOKEN>", token);
		requestURL = requestURL.replace("<LOCK_ID>", lockId);

		//Get keys from URL
		HttpRequest httprequest = new HttpRequest();
		String lockKey = httprequest.getFromURL(requestURL).trim();

		String direction = String.valueOf(lockKey.charAt(0));
		StringBuilder newKey = new StringBuilder(lockKey);

		if (direction.equals("0")) {
			newKey.setCharAt(0, '1');
		} else if (direction.equals("1")) {
			newKey.setCharAt(0, '0');
		}

		lockKey = newKey.toString();

		Toast.makeText(getApplicationContext(),
				lockKey, Toast.LENGTH_SHORT).show();

		Log.e("url", requestURL);

		// Set account with the keys the user is authorized to use
		AccountStorage.SetAccount(MainActivity.this, lockKey);

	}

}
