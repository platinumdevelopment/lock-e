package com.example.capstone;

import java.io.Serializable;

import android.app.ActionBar.LayoutParams;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.widget.TableRow;
import android.widget.TextView;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

public class Lock extends TextView implements Serializable{

	private String id;
	private String lockName;
	private int relationship;
	
	public Lock(Context context) {
		super(context);
		
		LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		this.setLayoutParams(params);
		
		this.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				
				Intent intent = new Intent(v.getContext(), OwnedLocks.class);
				intent.putExtra("lockId", id);
				intent.putExtra("lockName", lockName);
				intent.putExtra("relationship", relationship);
				
				v.getContext().startActivity(intent);				
			}
		});		
	}
	

	
	public String getLockId() {
		return id;
	}
	public String getLockName() {
		return lockName;
	}
	public int getRelationship(int relationship) {
		return relationship;
	}
	public void setLockId(String id) {
		this.id = id;
	}
	public void setLockName(String lockName) {
		this.lockName = lockName;
	}
	public void setRelationship(int relationship) {
		this.relationship = relationship;
	}
	
}
