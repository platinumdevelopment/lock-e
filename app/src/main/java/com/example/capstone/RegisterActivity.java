package com.example.capstone;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

public class RegisterActivity extends Activity {

	String registerUrl1 = "https://tranquil-caverns-74038.herokuapp.com/LocknLoad/addUser/email=";
	String registerUrl2 = "&password=";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.register, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

    //IF BACK BUTTON IS PRESSED
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

	public void register(View view) {

		EditText newEmail1_et = (EditText) findViewById(R.id.newEmail1);
		EditText newEmail2_et = (EditText) findViewById(R.id.newEmail2);

		EditText newPassword1_et = (EditText) findViewById(R.id.newPassword1);
		EditText newPassword2_et = (EditText) findViewById(R.id.newPassword2);

		String newEmail1 = newEmail1_et.getText().toString();
		String newEmail2 = newEmail2_et.getText().toString();

		String newPassword1 = newPassword1_et.getText().toString();
		String newPassword2 = newPassword2_et.getText().toString();
		
		Log.e("email1", newEmail1);
		Log.e("email2", newEmail2);
		Log.e("pass1", newPassword1);
		Log.e("pass2", newPassword2);

		

		if (newEmail1.equals("") || newEmail2.equals("")
				|| newPassword1.equals("") || newPassword2.equals("")) {

			Toast.makeText(getApplicationContext(), "Please fill in the form.",
					Toast.LENGTH_LONG).show();
		} else if (!(newEmail1.equals(newEmail2))
				|| !(newPassword1.equals(newPassword2))) {

			Toast.makeText(getApplicationContext(),
					"Emails and/or Passwords don't match", Toast.LENGTH_LONG)
					.show();
		} else {

			registerUrl1 = registerUrl1 + newEmail1 + registerUrl2 + newPassword1 + "/";
			
			HttpRequest httprequest = new HttpRequest();
			String http_return = "";
			
			http_return = httprequest.getFromURL(registerUrl1);
			
			// While http return value hasn't changed, loop until it does
			while (http_return.equals("")) {

				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}

			if (!http_return.startsWith("User with that email already exists!")) {

				Intent intent = new Intent(this, LoginActivity.class);
				startActivity(intent);
                Toast.makeText(getApplicationContext(),
                        "Account Successfully Created!", Toast.LENGTH_LONG).show();
				finish();
				
			} else {

				Toast.makeText(getApplicationContext(),
						"User with that email already exists!", Toast.LENGTH_LONG).show();

			}

					
		}
	}
}
