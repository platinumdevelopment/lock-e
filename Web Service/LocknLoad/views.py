import jwt
import base64
import os

from functools import wraps
from flask import Flask, request, jsonify, _request_ctx_stack
from werkzeug.local import LocalProxy
from flask.ext.cors import cross_origin

from django.shortcuts import render
from django.http import HttpResponse
from .models import *
from django.db.models import Q
import json
import datetime
from django.utils import timezone

app = Flask(__name__)
# Authentication annotation
current_user = LocalProxy(lambda: _request_ctx_stack.top.current_user)


def JWTTest(request):
	encoded = jwt.encode({'some': 'payload'}, 'secret', algorithm='HS256')
	return HttpResponse(encoded)


def index(request):
	return HttpResponse("Hello, world. LockNLoad")

def Login(request, email, password):
	user = User.objects.filter(email=email, password=password)
	if user.count() > 0:
		accessToken = jwt.encode({'user_id': str(user[0].id), 'exp': datetime.datetime.utcnow() + datetime.timedelta(minutes=30)}, 'secret', algorithm='HS256')
		return HttpResponse(accessToken)
	else:
		return HttpResponse("-1")

def AddUser(request, email, password):
	new_user, created = User.objects.get_or_create(email=email)
	if created:
		new_user.password = password
		new_user.save()
		return HttpResponse("User created successfully!")
	else:
		return HttpResponse("User with that email already exists!")

def AddLock(request, token, name, id, apdu, direction):
	owner = parseToken(token)
	if owner == "-1":
		return HttpResponse("-1")
	if Lock.objects.filter(lock_id=id).count() == 0:
		new_lock = Lock.objects.create(lock_id=int(id), owner=User.objects.get(id=owner), name=name, apdu=apdu, direction=direction)
		new_lock.save()
		LockAuthorization.objects.create(lock=new_lock, authorizing_user=User.objects.get(id=owner), authorized_user=User.objects.get(id=owner), relation=0)
		return HttpResponse("Lock registered successfully!")
	else:
		return HttpResponse("A lock with that id already exists")

def AddAuthorization(request, token, user_to_add, user_to_add_type, lock_id):
	user_requesting = parseToken(token)
	if user_requesting == "-1":
		return HttpResponse("-1")
	if User.objects.filter(email=user_to_add).count == 0:
		return HttpResponse("User doesn't exist!")
	if LockAuthorization.objects.filter(lock=Lock.objects.get(id=lock_id), authorized_user=User.objects.get(email=user_to_add)):
		return HttpResponse("User already authorized to use that lock!")
	if Lock.objects.filter(id=lock_id, owner=User.objects.get(id=user_requesting)).count() > 0:
		if user_to_add_type == "tenant":
			LockAuthorization.objects.create(lock=Lock.objects.get(id=lock_id), authorizing_user = User.objects.get(id=user_requesting), authorized_user=User.objects.get(email=user_to_add), relation=0)
			return HttpResponse("Successfully authorized tenant!")
		else:
			if getNumberTempKeys(lock_id) < 6:
				LockAuthorization.objects.create(lock=Lock.objects.get(id=lock_id), authorizing_user = User.objects.get(id=user_requesting), authorized_user=User.objects.get(email=user_to_add), relation=1, exp_time=datetime.datetime.now()+ datetime.timedelta(minutes=1440))
				return HttpResponse("Successfully authorized guest!")
			else:
				return HttpResponse("Too many temporary keys have already been issued for this lock.")
	else:
		if getNumberTempKeys(lock_id) < 6:
			LockAuthorization.objects.create(lock=Lock.objects.get(id=lock_id), authorizing_user = User.objects.get(id=user_requesting), authorized_user=User.objects.get(email=user_to_add), relation=2, exp_time=datetime.datetime.now() + datetime.timedelta(minutes=1440))
			return HttpResponse("Successfully authorized guest!")	
		else:
			return HttpResponse("Too many temporary keys have already been issued for this lock.")


def RevokeAuthorization(request, token, user_to_revoke, lock_id):
	if parseToken(token) == "-1":
		return HttpResponse("-1")
	LockAuthorization.objects.filter(lock=Lock.objects.get(id=lock_id), authorized_user=User.objects.get(id=user_to_revoke)).delete()
	LockAuthorization.objects.filter(lock=Lock.objects.get(id=lock_id), authorizing_user=User.objects.get(id=user_to_revoke)).delete()
	return HttpResponse("Successfully removed authorization!")

def getNumberTempKeys(lock_id):
	tempKeys = LockAuthorization.objects.filter(lock=Lock.objects.get(id=lock_id))
	activeTempKeys = 0
	for tempKey in tempKeys:
		if tempKey.exp_time is not None and tempKey.exp_time > timezone.now():
			activeTempKeys += 1
	return activeTempKeys

def RequestKey(request, token, lock_id):
	user_id = parseToken(token)
	if user_id == "-1":
		return HttpResponse("-1")
	if LockAuthorization.objects.filter(authorized_user=User.objects.get(id=user_id), lock=Lock.objects.get(id=lock_id)).count() > 0:
		return HttpResponse("Key")		

	return HttpResponse("-1")

def GetLocks(request, token):
	user_id = parseToken(token)
	if user_id == "-1":
		return HttpResponse("-1")
	locks = {}
	locks['owner'] = []
	locks['tenant'] = []
	locks['guest'] = []
	alreadyAdded = []
	for authorization in LockAuthorization.objects.filter(authorized_user=User.objects.get(id=user_id)):
		lock = {}
		lock['id'] = authorization.lock.id
		lock['name'] = authorization.lock.name
		lock['lock_id'] = authorization.lock.lock_id
		if lock['id'] not in alreadyAdded:
			alreadyAdded.append(lock['id'])
			if authorization.authorized_user == authorization.authorizing_user:
				locks['owner'].append(lock)
			elif authorization.relation == 0:
				locks['tenant'].append(lock)
			else:
				locks['guest'].append(lock)
	return HttpResponse(json.dumps(locks), content_type="application/json")
			

def GetUsersUnder(request, token, lock_id):
	usersUnder = {}
	usersUnder['tenants'] = []
	usersUnder['guests'] = []
	user_id = parseToken(token)
	if user_id == "-1":
		return HttpResponse("-1")
	for authorization in LockAuthorization.objects.filter(lock=Lock.objects.get(id=lock_id), authorizing_user=User.objects.get(id=user_id)):
		user = {}
		user['email'] = authorization.authorized_user.email
		user['id'] = authorization.authorized_user.id
		if authorization.authorizing_user != authorization.authorized_user:
			if authorization.relation == 1 or authorization.relation == 2:
				usersUnder['guests'].append(user)
			if authorization.relation == 0:
				usersUnder['tenants'].append(user)
	return HttpResponse(json.dumps(usersUnder), content_type="application/json")

def EncryptTest(request, string):
	return HttpResponse(Encrypt(string))


def DeleteLock(request, token, lock_id):
	if parseToken(token) == "-1":
		return HttpResponse("-1")
	LockAuthorization.objects.filter(lock=Lock.objects.get(id=lock_id)).delete();
	Lock.objects.get(id=lock_id).delete();
	return HttpResponse("Lock deleted successfully");

def Encrypt(lockKey):
        encryptedString = bytearray()
        encryptionKey = "y64yszya6ya4w64w632tuiog0"
        #encryptionKey = "11111111111111111111111111111"
	for i in range(0, len(lockKey)):
                encryptedString.append((ord(lockKey[i]) + ord(encryptionKey[i % len(lockKey)]))%256)
        return str(encryptedString)


def GetKeys(request, token):
        keys = "" 
	user_id = parseToken(token)
	if user_id == "-1":
		return HttpResponse("-1")
	alreadyAdded = []
        for authorization in LockAuthorization.objects.filter(authorized_user=User.objects.get(id=user_id)):
                if authorization.lock.lock_id not in alreadyAdded:
                        alreadyAdded.append(authorization.lock.lock_id)
        		keys += Encrypt(str(authorization.lock.direction) + str(authorization.lock.lock_id) + str(authorization.lock.apdu)) + ","
	return HttpResponse(keys[:-1])


def parseToken(token):
	try:
		decoded = jwt.decode(token, 'secret')
		return decoded['user_id']
	except jwt.ExpiredSignatureError:
		return -1
		 




