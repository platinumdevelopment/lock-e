from django.db import models

# Create your models here.
class User(models.Model):
	email = models.CharField(max_length=256)
	password = models.CharField(max_length=256)

class Lock(models.Model):
	owner = models.ForeignKey(User)
	name = models.CharField(max_length=256,default="unknown")
	lock_id = models.CharField(max_length=256,default="unknown")
	direction = models.IntegerField()
	apdu = models.CharField(max_length=5, default="unknown")
	
# add field for unique lock ID

class LockAuthorization(models.Model):
	lock = models.ForeignKey(Lock)
	authorizing_user = models.ForeignKey(User, related_name="authorizing_user")
	authorized_user = models.ForeignKey(User, related_name="authorized_user")
	relation = models.IntegerField()	# 0: guest-owner, 1: guest-tenant, 2:tenant-guest
	exp_time = models.DateTimeField(null=True)	

