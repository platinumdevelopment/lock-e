from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^JWTTest/$', views.JWTTest, name='JWTTest'),
    url(r'^encryptionTest/token=(?P<token>.+)&string=(?P<string>.+)/$', views.EncryptTest, name='EncryptTest'),
    url(r'^getLocks/token=(?P<token>.+)/$', views.GetLocks, name='GetLocks'),
    url(r'^getKeys/token=(?P<token>.+)/$', views.GetKeys, name='GetKeys'),
    url(r'^deleteLock/token=(?P<token>.+)&lock_id=(?P<lock_id>\d+)/$', views.DeleteLock, name='DeleteLock'),
    url(r'^getUsersUnder/token=(?P<token>.+)&lock_id=(?P<lock_id>\d+)/$', views.GetUsersUnder, name='GetUsersUnder'),
    url(r'^addUser/email=(?P<email>.*)&password=(?P<password>.*)/$', views.AddUser, name='AddUser'),
    url(r'^addLock/token=(?P<token>.+)&name=(?P<name>.*)&id=(?P<id>\d+)&apdu=(?P<apdu>.*)&direction=(?P<direction>.*)/$', views.AddLock, name='AddLock'),
    url(r'^addAuthorization/token=(?P<token>.+)&user_to_add=(?P<user_to_add>.*)&user_to_add_type=(?P<user_to_add_type>.*)&lock_id=(?P<lock_id>\d+)/$', views.AddAuthorization, name='AddAuthorization'),
    url(r'^revokeAuthorization/token=(?P<token>.+)&user_to_revoke=(?P<user_to_revoke>\d+)&lock_id=(?P<lock_id>\d+)/$', views.RevokeAuthorization, name='RevokeAuthorization'),
    url(r'^login/email=(?P<email>.*)&password=(?P<password>.*)/$', views.Login, name='Login'),
]
